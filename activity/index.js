console.log("Hello, World!");

let trainer = {
    name: "Chaz",
    age: 10,
    pokemon: ["Pickachu","Piplup","Infernape","Lucario","Jirachi"],
    friends: {
        sinnoh: ["Dawn","Cynthia"],
        kanto: ["Misty","Professor Oak"]
    },
    talk: function(){
        console.log("Pickachu! I choose you!");
    }
};
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();

function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = level * 3;
    this.attack = level * 1.5;
    this.tackle = function(pokemon){
        pokemon.health = pokemon.health - this.attack;
        console.log(name +" tackled " +pokemon.name);

        if(pokemon.health <= 0){
            console.log(pokemon.name +" hp: " +pokemon.health);
            pokemon.faint();
            console.log(this.name +" won!");
        } else {
            console.log(pokemon.name +" hp: " +pokemon.health);
        }
    }
    this.faint = function(){
        console.log(name +" has fainted!");
    }
}

let myPokemon = new Pokemon(trainer.pokemon[0], 100);
let pokemon1 = new Pokemon("Vaporeon", 90);
let pokemon2 = new Pokemon("Jolteon", 24);
let pokemon3 = new Pokemon("Flareon", 60);
let pokemon4 = new Pokemon("Umbreon", 43);
let pokemon5 = new Pokemon("Espeon", 39);
let pokemon6 = new Pokemon("Glaceon", 19);
let pokemon7 = new Pokemon("Leafeon", 81);
let pokemon8 = new Pokemon("Sylveon", 89);

console.log(myPokemon);
console.log(pokemon1);

myPokemon.tackle(pokemon1);
pokemon1.tackle(myPokemon);
myPokemon.tackle(pokemon1);

console.log(myPokemon);
console.log(pokemon8);

pokemon8.tackle(myPokemon);
myPokemon.tackle(pokemon8);
pokemon8.tackle(myPokemon);

