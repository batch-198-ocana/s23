// objects : use to describe something with characteristics
let dog = {
    breed: "Husky",
    color: "White",
    legs: 4,
    isHuggable: true,
    isLoyal: true
};
console.log(dog);

//mini activity
let favoriteVideoGame = {
    title: "Genshin Impact",
    publisher: "Hoyoverse",
    year: 2020,
    price: "free",
    director: "none",
    isAvailable: true
}
console.log(favoriteVideoGame);

favoriteVideoGame.title = "Final Fantasy X";
favoriteVideoGame.publisher = "Square Enix";
favoriteVideoGame.year = 2001;

console.log(favoriteVideoGame);

let course = {
    title: "Philosophy 101",
    description: "Learn the values of life",
    price: 5000,
    isActive: true,
    instructors: ["Mr. Johnson","Mrs. Smith","Mr.Francis"]
};

course.instructors.pop();
course.instructors.push("Mr. McGee");
console.log(course.instructors);

let isPresent = course.instructors.includes("Mr. Johnson");
console.log(isPresent);

function addNewInstructor(instructor){
    if(course.instructors.includes(instructor)){
        console.log("Instructor already added.")
    } else {
        course.instructors.push(instructor);
        console.log("Thank you. Instructor added")
    }
}
addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Marco");
console.log(course.instructors);

let instructor = {};
console.log(instructor);

instructor.name = "James Johnson";
instructor.age = 56;
instructor.gender = "male";
instructor.department = "Humanities";
instructor.salary = 50000;
instructor.teaches = ["Philosophy","Humanities","Logic"];
console.log(instructor);

instructor.address = {
    street: "#1 Maginhawa St.",
    city: "Quezon City",
    country: "Philippines"
}

console.log(instructor.address.street);

// constructor function
function superHero(name, superpower, powerLevel){
    // this keyword when added in a constructor function refers to the object that will be made by the function
    /*
        {
            name: <valueOfParameterName>,
            superpower: <valueOfParameterSuperpower>,
            powerlevel: <valueOfParameterPowerLevel>,
        }
    */
    this.name = name;
    this.superpower = superpower;
    this.powerlevel = powerLevel;
}
// new keyword allows us to create a new object out of our function
let superhero1 = new superHero("Saitama","One Punch",30000);
console.log(superhero1);

// mini activity
function laptops(name, brand, price){
    this.name = name;
    this.brand = brand;
    this.price = price;
}

let laptop1 = new laptops("acer model 1", "Acer",25000);
let laptop2 = new laptops("lenovo model x", "Lenovo",40000);
console.log(laptop1);
console.log(laptop2);

//object methods : functions that are associated with an object.
